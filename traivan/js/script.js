var shuffleme = (function( $ ) {
  'use strict';
  var $grid = $('#grid'),
      $filterOptions = $('.portfolio-sorting li'),
      $sizer = $grid.find('.shuffle_sizer'),

  init = function() {

    setTimeout(function() {
      listen();
      setupFilters();
    }, 100);

    $grid.shuffle({
      itemSelector: '[class*="col-"]',
      sizer: $sizer    
    });
  },

  setupFilters = function() {
    var $btns = $filterOptions.children();
    $btns.on('click', function(e) {
      e.preventDefault();
      var $this = $(this),
          isActive = $this.hasClass( 'active' ),
          group = $this.data('group');

      $grid.shuffle( 'shuffle', group );
    });

    $btns = null;
  },

  listen = function() {
    var debouncedLayout = $.throttle( 400, function() {
      $grid.shuffle('update');
    });

    $grid.find('img').each(function() {
      var proxyImage;

      if ( this.complete && this.naturalWidth !== undefined ) {
        return;
      }

      proxyImage = new Image();
      $( proxyImage ).on('load', function() {
        $(this).off('load');
        debouncedLayout();
      });

      proxyImage.src = this.src;
    });

    setTimeout(function() {
      debouncedLayout();
    }, 800);
  };      

  return {
    init: init
  };
}( jQuery ));

$(document).ready(function()
{
  shuffleme.init();
});

$(document).ready(function () {
    $('ul.portfolio-sorting li a').mousedown(function(e) {

        $('ul.portfolio-sorting li').removeClass('active');

        var $parent = $(this).parent();
        if (!$parent.hasClass('active')) {
            $parent.addClass('active');
        }
    });
});

function initMap() {
		var uluru = {lat: 27.69856, lng: 85.28863};  
        var map = new google.maps.Map(document.getElementById('map'), {
          center: uluru,
		  scrollwheel: false,
          zoom: 15,
          styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
        });
		var marker = new google.maps.Marker({
          position: uluru,
          map: map,
		  icon: {
		url: "../images/marker.png",
		scaledSize: new google.maps.Size(64, 88)
	},
		  title: 'Our Location'
        });
		
		google.maps.event.addDomListener(window, 'resize', function() {
          var center = map.getCenter()
          google.maps.event.trigger(map, "resize")
          map.setCenter(center)
        })
		
		var contentString = '<div id="content">'+
            '<p>Yup, we are here!</p>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
		infowindow.open(map,marker);
		google.maps.event.trigger(map, 'resize');

}


$(function(){
	$.stellar({
		horizontalOffset: 130,
		verticalOffset: -28
	});
});
		
$( "#mobile-menu" ).on( "click", function() {
  $( this ).toggleClass( "active" );
});


function clickFunction() {
    document.getElementById("mobile-dropdown").classList.toggle("show");
	document.getElementById("push").classList.toggle("move");
	document.getElementById("display").classList.toggle("no-display");
	document.getElementById("push-img").classList.toggle("about-img-move");
}

 var $container = $('.masonry');
$container.imagesLoaded( function(){
  $container.masonry({
    itemSelector : '.masonry-item'
  });
});

